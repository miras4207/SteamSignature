<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="sig.css" type="text/css">
</head>

<?php
	$status = $steam->getStatus();
	switch ($status)
	{
		case SteamSignature::STATUS_ONLINE: $statusClass = "online"; break;
		case SteamSignature::STATUS_INGAME: $statusClass = "ingame"; break;
		default: 							$statusClass = "offline"; break;
	}
?>


<div class="steamsig <?php echo $statusClass;?>" onclick="window.open('<?php echo $this->getProfileUrl();?>','steam_profile');" style="cursor: pointer;">
	<div class="avatar"><img src="<?php echo $steam->getAvatarFull();?>"></div>	
	<div class="topline">
		<div class="nickname"><?php echo $steam->getNickname();?></div>
		<div class="country"><?php echo $steam->getCountryFlag();?></div>
	</div>
	<div class="middleline">
		<div class="status">
		<?php
			if(!$steam->isPublic())
			{
				echo "<span class='private'>This profile is private</span>";
			}
			else if($status == SteamSignature::STATUS_ONLINE)
			{
				echo "Currently Online";
			}
			else if($status == SteamSignature::STATUS_INGAME)
			{
				echo $steam->getInGameStatus();
			}
			else
			{
				echo "Currently Offline";
			}
		?>
		</div>
	</div>

	<div class="bottomline">

	<?php
		if(!$steam->isPublic())
		{
			echo "";
		}
		else if($status == SteamSignature::STATUS_ONLINE)
		{
			echo $steam->getTotalTimePlayed();
		}
		else if ($status == SteamSignature::STATUS_INGAME)
		{
			echo $steam->getCurrentGameTimePlayed();
		}
		else
		{
			echo $this->getLastOnline();
		}		
	?>

	</div>
</div>