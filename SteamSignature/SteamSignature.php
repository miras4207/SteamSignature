<?php
class SteamSignature
{
	private static $API_KEY;

	protected static $expected_properties = array(
											"steamid" 	  				=> false,
											"personaname" 				=> false,
											"profileurl"  				=> false,
											"avatar"	  				=> false,
											"avatarmedium"				=> false,
											"avatarfull"				=> false,
											"personastate"				=> true,
											"communityvisibilitystate"	=> true,
											"profilestate"				=> true,
											"lastlogoff"				=> false,
											"commentpermission"			=> false,
										  );
	public static $forceValidate = false;

	CONST FORMAT = "json";
	CONST URL = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=%API_KEY%&format=%FORMAT%&steamids=%STEAM_ID%";
	CONST URL_GAMES = "http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key=%API_KEY%&steamid=%STEAM_ID%&format=%FORMAT%&include_appinfo=1";
	CONST API_KEY_FILE = "SteamAPI.txt";

	CONST STATUS_OFFLINE 			= 0;
	CONST STATUS_ONLINE  			= 1;
	CONST STATUS_INGAME  			= 11;
	CONST STATUS_BUSY				= 2;
	CONST STATUS_AWAY				= 3;
	CONST STATUS_SNOOZE				= 4;
	CONST STATUS_LOOKING_FOR_TRADE	= 5;
	CONST STATUS_LOOKING_FOR_GAME	= 6;

	CONST PUBLIC_STATUS_HIDDEN 		= 1;
	CONST PUBLIC_STATUS_VISIBLE     = 3;

	CONST MAX_LINE_LENGTH			= 35;

	protected $steam64Id;
	public $api_content;
	public $games_content;
	public $view;

	public function __construct ($steam64Id, $view = "", $useApiFromFile = true)
	{
	    $this->steam64Id = $steam64Id;
	    $this->view = $view;

	    $api_file = dirname(__FILE__) . DIRECTORY_SEPARATOR . SteamSignature::API_KEY_FILE;
	    if(empty(self::$API_KEY) && file_exists($api_file) && $useApiFromFile)
	    {
	    	SteamSignature::setAPI(trim(file_get_contents($api_file)));
	    }
	    
	    if(!$this->updateContent())
	    	self::log(__METHOD__, "Failed to load Steam API Data", "error");
  	}

  	public static function log ($method, $message, $level)
	{
		echo "<br>" . $method . " -> " . $level . ": " . $message;
	}

	public static function setAPI($key_API)
	{
		self::$API_KEY = $key_API;
	}

	public function getUrl()
	{
		$url = SteamSignature::URL;
		$url = str_replace("%API_KEY%",  self::$API_KEY, $url);
		$url = str_replace("%FORMAT%",   SteamSignature::FORMAT,  $url);
		$url = str_replace("%STEAM_ID%", $this->steam64Id, $url);
		return $url;
	}

	public function getGamesUrl()
	{
		$url = SteamSignature::URL_GAMES;
		$url = str_replace("%API_KEY%",  self::$API_KEY, $url);
		$url = str_replace("%FORMAT%",   SteamSignature::FORMAT,  $url);
		$url = str_replace("%STEAM_ID%", $this->steam64Id, $url);
		return $url;
	}

	public function setFormat($format)
	{
		$this->FORMAT = $format;
	}

	public function updateGameContent()
	{
		$this->games_content = json_decode(file_get_contents($this->getGamesUrl()), TRUE);
		if(isset($this->games_content["response"]))
		{
			$this->games_content = $this->games_content["response"];
			return true;
		}
		else
		{
			return false;
		}
	}

	public function updateContent()
	{
		$this->api_content = json_decode(file_get_contents($this->getUrl()), TRUE);
		if(isset($this->api_content["response"]["players"][0]))
		{
			$this->api_content = $this->api_content["response"]["players"][0];
			if(self::$forceValidate && !$this->isValid())
			{
				$this->renderError("Invalid API Data");
				return false;
			}
			return true;
		}
		else
		{
			if(self::$forceValidate && !$this->isValid())
			{
				$this->renderError("Invalid API Data");
				return false;
			}
			return false;
		}
	}

	private function checkProperty($property_name, $canBeEmpty)
	{
		if(!isset($this->api_content[$property_name]))
			return false;
		if(empty($this->api_content[$property_name]) && $canBeEmpty == false)
			return false;
		return true;
	}

	public function isValid()
	{
		foreach (self::$expected_properties as $property => $canBeEmpty)
		{
			if(!$this->checkProperty($property, $canBeEmpty))
				return false;
		}
		return true;
	}

	public function render($customArgs = Array())
	{
		if(empty($this->view))
		{
			return $this->renderError("View is not defined");
		}

	    ob_start();

	    $customArgs["steam"] = $this;
	    extract($customArgs, EXTR_SKIP);
	    
	    include $this->view;
		$data = ob_get_contents();

		ob_get_clean();

	    return $data;
	}

	public function renderError($error_message = "Unknown error")
	{
		echo "Steam Sig Error $error_message";
	}

	public function getNickname()
	{
		return $this->api_content["personaname"];
	}

	public function getSteam64Id()
	{
		return $this->api_content["steamid"];
	}

	public function getProfileUrl()
	{
		return $this->api_content["profileurl"];
	}

	public function getAvatar()
	{
		return $this->api_content["avatar"];
	}

	public function getAvatarMedium()
	{
		return $this->api_content["avatarmedium"];
	}

	public function getAvatarFull()
	{
		return $this->api_content["avatarfull"];
	}

	public function getCommunityState()
	{
		return $this->api_content["communityvisibilitystate"];
	}

	public function getProfileState()
	{
		return $this->api_content["profilestate"];
	}

	public function getCurrentGameName()
	{
		if(empty($this->api_content["gameextrainfo"]))
			return null;
		else
			return $this->api_content["gameextrainfo"];
	}

	public function getCurrentGameId()
	{
		if(empty($this->api_content["gameid"]))
			return null;
		else
			return $this->api_content["gameid"];
	}

	public function getCurrentGameTimePlayed()
	{
		if(empty($this->games_content))
			$this->updateGameContent();

		if(empty($this->api_content["gameid"]))
			return null;

		if(empty($this->games_content))
			return null;

		$id = $this->api_content["gameid"];
		$minutes = 0;

		foreach ($this->games_content["games"] as $game => $info)
		{
			if($info["appid"] == $id)
			{
				$minutes = $info["playtime_forever"];
				break;
			}
		}

		if(empty($minutes))
			return null;		

		$d = floor ($minutes / 1440);
		$h = floor (($minutes - $d * 1440) / 60);
		$m = $minutes - ($d * 1440) - ($h * 60);

		$str = "";
		if(!empty($d))
			$str .= "$d d ";
		if(!empty($h))
			$str .= "$h h ";
		if(!empty($m))
			$str .= "$m m ";

		return "Game played for: " . $str;
	}

	public function getInGameStatus()
	{
		$game = $this->getCurrentGameName();
		if(empty($game))
			return mb_strimwidth("Currently In-Game", 0, SteamSignature::MAX_LINE_LENGTH, "...");
		else
			return mb_strimwidth("Playing $game", 0, SteamSignature::MAX_LINE_LENGTH, "...");
	}

	public function getLastLogoffDate($format = "Y-m-d H:i")
	{
		return date($format, $this->api_content["lastlogoff"]);
	}

	public function getLastLogoff()
	{
		if(!empty($this->api_content["lastlogoff"]))
			return $this->api_content["lastlogoff"];
		else
			return null;
	}

	public function getLastOnline()
	{
		$time_off = $this->getLastLogoff();

		if(empty($time_off))
			return null;

		$time_now = time();

		$diff = (intval($time_now) - intval($time_off)) / 60; //Duration in minutes

		$d = floor ($diff / 1440);
		$h = floor (($diff - $d * 1440) / 60);
		$m = $diff - ($d * 1440) - ($h * 60);

		if($d >= 1)
		{
			if($d > 365)
			{
				return "Last Online: more than 1 year ago";
			}
			else if($d == 1)
				return "Last Online: $d day ago";
			else
				return "Last Online: $d days ago";
		}
		else if ($h >= 1)
		{
			if($h == 1)
				return "Last Online: $h hour ago";
			else
				return "Last Online: $h hours ago";
		}
		else
		{
			if($m == 1)
				return "Last Online: $m minute ago";
			else
				return "Last Online: $h minutes ago";
		}



	}

	public function getCommentsPermission()
	{
		return $this->api_content["commentpermission"];
	}

	public function getCountryFlag($id = "", $class="")
	{
		if(!empty($this->api_content["loccountrycode"]))
		{
			return "<img src='http://projects.mirascodes.com/AdminBot/ETSAM/flags/".strtolower($this->api_content["loccountrycode"]).".png' id='$id' class='$class'>";
		}
		else
			return "";
	}

	public function getStatus()
	{
		if(!$this->isOnline())
			return SteamSignature::STATUS_OFFLINE;
		
		if(!empty($this->api_content["gameid"]))
			return SteamSignature::STATUS_INGAME;

		return SteamSignature::STATUS_ONLINE;
	}

	public function isPublic()
	{
		if($this->api_content["communityvisibilitystate"] == SteamSignature::PUBLIC_STATUS_HIDDEN)
			return false;
		else
			return true;
	}

	public function isOnline()
	{
		if($this->api_content["personastate"] == SteamSignature::STATUS_OFFLINE)
			return false;
		return true;
	}

	public function getGameCount()
	{
		if(empty($this->games_content))
			$this->updateGameContent();

		if(isset($this->games_content["game_count"]))
			return $this->games_content["game_count"];
		else
			return null;
	}

	public function getTotalTimePlayed()
	{
		if(empty($this->games_content))
			$this->updateGameContent();

		if($this->getGameCount() != null)
		{
			$total_minutes = "";
			foreach ($this->games_content["games"] as $game => $info)
			{
				$total_minutes += intval($info["playtime_forever"]);
			}

			$d = floor ($total_minutes / 1440);
			$h = floor (($total_minutes - $d * 1440) / 60);
			$m = $total_minutes - ($d * 1440) - ($h * 60);

			$str = "";
			if(!empty($d))
				$str .= "$d days ";
			if(!empty($h))
				$str .= "$h hours ";
			if(!empty($m))
				$str .= "$m minutes ";

			return "Time played: " . $str;

		}
		else
			return null;

	}
}